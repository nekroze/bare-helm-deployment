{nixpkgs ? import <nixpkgs> {}}:
with nixpkgs;

stdenv.mkDerivation rec {
  name = "helm-app";
  version = "1.0.0";
  src = ./.;

  buildInputs = [
    gitMinimal
    minikube
    kubernetes-helm
    kubernetes
  ];

  shellHook = ''
    alias gcd="cd $PWD"

    function up() {
      minikube up
      helm init
    }

    function down() {
      minikube delete
    }
  '';
}
